*** Settings ***
Documentation    Strings are uppercasable
Resource	my_keywords.resource

*** Test Cases ***

a string can be uppercased
    Given String "a" is set to "azerty"
    And String "b" is set to "AZERTY"
    When String "a" is uppercased
    Then String "a" and "b" are equal
