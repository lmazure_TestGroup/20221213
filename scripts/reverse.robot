*** Settings ***
Documentation    Strings are revertable
Resource	my_keywords.resource

*** Test Cases ***

a string can be inverted
    When String "a" is set to "azerty"
    when string "b" is set to "ytreza"
    When String "a" is reverted
    Then String "a" and "b" are equal
